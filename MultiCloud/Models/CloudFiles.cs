﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dropbox.Api.Files;

namespace MultiCloud.Models
{
    public class CloudFiles
    {
        public string FileID { get; private set; }
        public string FileName { get; private set; }
        public ulong FileSize { get; private set; }
        public string FileCreateDate { get; private set; }
        public string FileModifiedDate { get; private set; }

        public static CloudFiles Parse(string fileid, string filename, ulong filesize,
                                        DateTime filecreatedate, DateTime filemodifieddate)
        {
            return new CloudFiles
            {
                FileID = fileid,
                FileName = filename,
                FileSize = filesize,
                FileCreateDate = filecreatedate.ToShortDateString(),
                FileModifiedDate = filemodifieddate.ToShortDateString(),
            };
        }
    }

    public class CloudFilesView
    {
        public string FileID { get; private set; }
        public string FileName { get; private set; }
        public ulong FileSize { get; private set; }
        public string FileCreateDate { get; private set; }
        public string FileModifiedDate { get; private set; }
        public string CloudState { get; private set; }

        public static CloudFilesView Parse(string fileid, string filename, ulong filesize,
                                        DateTime filecreatedate, DateTime filemodifieddate, string cloudstate)
        {
            return new CloudFilesView
            {
                FileID = fileid,
                FileName = filename,
                FileSize = filesize,
                FileCreateDate = filecreatedate.ToShortDateString(),
                FileModifiedDate = filemodifieddate.ToShortDateString(),
                CloudState = cloudstate
            };
        }

        public string BytesToString(long byteCount)
        {
            string[] suf = { "B", "KB", "MB", "GB", "TB", "PB", "EB" }; //Longs run out around EB
            if (byteCount == 0)
                return "0" + suf[0];
            long bytes = Math.Abs(byteCount);
            int place = Convert.ToInt32(Math.Floor(Math.Log(bytes, 1024)));
            double num = Math.Round(bytes / Math.Pow(1024, place), 1);
            return (Math.Sign(byteCount) * num).ToString() + suf[place];
        }
    }

    public class DropboxFile
    {
        public MediaMetadata mediameta { get; private set; }
        public string example { get; private set; }
    }

    public class OnedriveFile
    {
        public string example { get; set; }
    }

    public class GoogleDriveFile
    {
        public string example { get; set; }
    }
}