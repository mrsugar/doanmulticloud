﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


using Dropbox.Api;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Auth.OAuth2.Flows;
using Google.Apis.Auth.OAuth2.Mvc;
using Google.Apis.Auth.OAuth2.Mvc.Controllers;
using Google.Apis.Drive.v3;
using Google.Apis.Util.Store;
using System.Web.Mvc;
using Google.Apis.Services;
using System.Threading.Tasks;
using Dropbox.Api.Files;


namespace MultiCloud.Models
{
    public class CloudProviders
    {

        // Dropbox
        public static string DropboxAPIKey = "i6eg67w9ntxztua";
        public static string DropboxAPISecret = "gp0w5458l1mm95q";
        public static string DropboxAuthURL = "https://corgiwp.com/Cloud/DropboxAuth";

        public static DropboxClient GetClient(string Token)
        {
            if (Token == null || string.IsNullOrWhiteSpace(Token))
            {
                return null;
            }
            return new DropboxClient(Token);
        }

        public async Task GetThumbnail(string path, DropboxClient client)
        {
            using (var response = await client.Files.GetThumbnailAsync("", null, ThumbnailSize.W64h64.Instance))
            {
                using (var fileStream = System.IO.File.Create(("~/Test/tuan.jpg")))
                {
                    var thumbStream = await response.GetContentAsStreamAsync();
                    thumbStream.CopyTo(fileStream);
                }
            }
        }

        // google drive

        public static DriveService GetClient(Google.Apis.Http.IConfigurableHttpClientInitializer Token)
        {
            return new DriveService(new BaseClientService.Initializer
            {
                HttpClientInitializer = Token,
                ApplicationName = "ASP.NET MVC Sample"
            });
        }
       
    }

    public class AppFlowMetadata : FlowMetadata
    {
        private static string folder = System.Web.HttpContext.Current.Server.MapPath("/App_Data/GoogleStore");
        private static readonly IAuthorizationCodeFlow flow =
            new GoogleAuthorizationCodeFlow(new GoogleAuthorizationCodeFlow.Initializer
            {
                ClientSecrets = new ClientSecrets
                {
                    ClientId = "109802825419-2p7enc15ubvm4kq6bk4ufrnl8bvu2ff3.apps.googleusercontent.com",
                    ClientSecret = "PdKrbHo2-Wmx8Drb1M2suZep"
                },
                Scopes = new[] { DriveService.Scope.Drive },
                DataStore = new FileDataStore(folder)
            });

        public override string GetUserId(Controller controller)
        {
            // In this sample we use the session to store the user identifiers.
            // That's not the best practice, because you should have a logic to identify
            // a user. You might want to use "OpenID Connect".
            // You can read more about the protocol in the following link:
            // https://developers.google.com/accounts/docs/OAuth2Login.
            var user = controller.Session["user"];
            if (user == null)
            {
                user = Guid.NewGuid();
                controller.Session["user"] = user;
            }
            return user.ToString();
        }



        
        public override IAuthorizationCodeFlow Flow
        {
            get { return flow; }
        }
    }

}