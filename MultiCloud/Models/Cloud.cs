//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MultiCloud.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Cloud
    {
        public int CloudID { get; set; }
        public int UserID { get; set; }
        public string CloudToken { get; set; }
        public string CloudState { get; set; }
    }

}
