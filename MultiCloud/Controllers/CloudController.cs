﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.IO;

using Dropbox.Api;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Auth.OAuth2.Flows;
using Google.Apis.Auth.OAuth2.Mvc;
using Google.Apis.Drive.v3;
using Google.Apis.Util.Store;
using Google.Apis.Services;

using MultiCloud.Models;
using Dropbox.Api.Files;

namespace MultiCloud.Controllers
{
    public class CloudController : Controller
    {
        private CloudDatabaseEntities db = new CloudDatabaseEntities();

        private string ConnectState;

        public int UserID
        {
            get
            {
                return db.Users.Single(u => u.Email == User.Identity.Name).UserID;
            }
        }

        [Authorize]
        public async Task<ActionResult> DriveConnect(CancellationToken cancellationToken)
        {
            var result = await new AuthorizationCodeMvcApp(this, new AppFlowMetadata()).AuthorizeAsync(cancellationToken);

            if (result.Credential != null)
            {
                var service = CloudProviders.GetClient(result.Credential);
                return View();
            }
            else
            {
                return new RedirectResult(result.RedirectUri);
            }

        }

        [Authorize]
        public async Task<ActionResult> DriveAuth(string code, string state)
        {
            return View(state + code);
        }

        [Authorize]
        public ActionResult DropboxConnect()
        {
            this.ConnectState = Guid.NewGuid().ToString("N");
            var redirect = DropboxOAuth2Helper.GetAuthorizeUri(
                OAuthResponseType.Code,
                CloudProviders.DropboxAPIKey,
                CloudProviders.DropboxAuthURL,
                this.ConnectState);
            return Redirect(redirect.ToString());
        }

        [Authorize]
        public async Task<ActionResult> DropboxAuth(string code, string state)
        {
            try
            {
                var response = await DropboxOAuth2Helper.ProcessCodeFlowAsync(
                    code,
                    CloudProviders.DropboxAPIKey,
                    CloudProviders.DropboxAPISecret,
                    CloudProviders.DropboxAuthURL);

                // Try to check CloudState (Uid) for duplicate dropbox users

                if (await db.Clouds.AnyAsync(c => c.CloudState == response.Uid) == true)
                {
                    ViewBag.Error = "Bạn đã kết nối với tài khoản này rồi, vui lòng thoát tài khoản hoặc hủy kết nối";
                }
                else
                {
                    Cloud _cloud = db.Clouds.Add(new Cloud { CloudToken = response.AccessToken, CloudState = response.Uid, UserID = UserID });
                    await db.SaveChangesAsync();
                }
                
                return RedirectToAction("Index", "Cloud");
            }
            catch(Exception e)
            {
                return View(e.Data + e.Message);
            }
        }
        // GET: Cloud
        [Authorize]
        public ActionResult Index()
        {
            return View(db.Clouds.Where(c => c.UserID == UserID).ToList());
        }

        [Authorize]
        public async Task<ActionResult> Delete(int? state)
        {
            if (state == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cloud cloud = await db.Clouds.FindAsync(db.Clouds.Single(c => c.CloudState == state.ToString()).CloudID);
            if (cloud == null)
            {
                return HttpNotFound();
            }
            return View(cloud);
        }

        // POST: Account/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int state)
        {
            Cloud cloud = await db.Clouds.FindAsync(db.Clouds.Single(c => c.CloudState == state.ToString()).CloudID);
            db.Clouds.Remove(cloud);
            await db.SaveChangesAsync();
            return RedirectToAction("Index", "Cloud");
        }

        [Authorize]
        public async Task<ActionResult> Files()
        {
            var Files = new List<CloudFilesView>();
            foreach (Cloud cloud in db.Clouds.Where(c => c.UserID == UserID))
            {
                DropboxClient client = CloudProviders.GetClient(cloud.CloudToken);
                var List = await client.Files.ListFolderAsync(path: "");
                foreach (var item in List.Entries)
                {
                    if (!item.IsFile) continue; // Please, we only help you manage file, not folder :))
                    var files = item.AsFile;
                    var filemeta = CloudFilesView.Parse(fileid: files.Id, filename: item.Name, filesize: files.Size,
                        filecreatedate: files.ClientModified, filemodifieddate: files.ServerModified, cloudstate: cloud.CloudState);
                    
                    if (filemeta != null) Files.Add(filemeta);
                }
            }
            return View(Files.ToList());
        }

        [Authorize]
        public async Task<ActionResult> Download(string fileid, string state, string provider)
        {
            var Files = new List<CloudFiles>();

            Cloud cloud = await db.Clouds.FirstOrDefaultAsync(c => c.CloudState == state);
            if (cloud != null)
            {
                DropboxClient client = CloudProviders.GetClient(cloud.CloudToken);
                var File = await client.Files.GetTemporaryLinkAsync(fileid);
                if (File != null) return Redirect(File.Link);
            }
            else
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            
            return View();
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

      
    }

    public class AuthCallbackController : Google.Apis.Auth.OAuth2.Mvc.Controllers.AuthCallbackController
    {
        protected override Google.Apis.Auth.OAuth2.Mvc.FlowMetadata FlowData
        {
            get { return new AppFlowMetadata(); }
        }
    }

}