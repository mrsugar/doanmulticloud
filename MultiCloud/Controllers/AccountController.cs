﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Helpers;
using System.Web.Security;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Text;
using System.Security.Cryptography;
using MultiCloud.Models;
using System.Threading;


namespace MultiCloud.Controllers
{
    public class AccountController : Controller
    {
        private CloudDatabaseEntities db = new CloudDatabaseEntities();

        // GET: Account
        [Authorize]
        public async Task<ActionResult> Index()
        {
            return View(await db.Users.ToListAsync());
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login", "Account");
        }

        // GET: Account/Login
        [AllowAnonymous]
        public ActionResult Login(string ReturnUrl)
        {
            ViewBag.ReturnUrl = ReturnUrl;
            if (User.Identity.IsAuthenticated) return RedirectToAction("Index", "Account");
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login([Bind(Include = "Email,Password,RememberMe")] LoginViewUser user, string ReturnUrl)
        {
            if (ModelState.IsValid)
            {
                var passwordBytes = Encoding.UTF8.GetBytes(user.Password);
                byte[] hashBytes;
                using (var sha = new SHA512Managed())
                {
                    hashBytes = sha.ComputeHash(passwordBytes);
                }
                var sb = new StringBuilder(hashBytes.Length * 2);
                foreach (var b in hashBytes)
                    sb.AppendFormat("{0:x2}", b);
                var hashStr = sb.ToString();

                var isValidAccount = db.Users.FirstOrDefault(u => u.Email == user.Email && u.Password == hashStr);

                if (isValidAccount != null)
                {
                    FormsAuthentication.SetAuthCookie(user.Email, true);
                    if (ReturnUrl != null) return Redirect(ReturnUrl);
                    return RedirectToAction("Index", "Account");
                }
                else
                {
                    ModelState.AddModelError("", "Sai tên đăng nhập hoặc mật khẩu");
                }
            }
            return View(user);
        }

        // GET: Account/Register
        [AllowAnonymous]
        public ActionResult Register()
        {
            if (User.Identity.IsAuthenticated) return RedirectToAction("Index");
            return View();
        }

        // POST: Account/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register([Bind(Include = "UserID,Email,Password,Gender,RememberMe,Verified")] RegisterViewUser user)
        {
            if (ModelState.IsValid)
            {
                if (await db.Users.AnyAsync(u => u.Email == user.Email.Trim()) == false)
                {
                    FormsAuthentication.SetAuthCookie(user.Email, true);
                    user.createDate = DateTime.Now;
                    user.Password = Crypto.Hash(user.Password, "SHA512");

                    db.Users.Add(new User { Email = user.Email,
                                            Password = user.Password,
                                            Gender = user.Gender,
                                            RememberMe = true,
                                            Verified = false,
                                            createDate = user.createDate
                                            });
                    await db.SaveChangesAsync();
                    return RedirectToAction("Index", "Account");
                }
                else
                {
                    ModelState.AddModelError("Email", "Địa chỉ email đã được đăng ký. Bạn có thể chuyển đến trang đăng nhập");
                }
            }
            return View();
        }

        // GET: Account/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = await db.Users.FindAsync(id);

            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: Account/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "UserID,Email,Password,Gender")] User user)
        {
            if (ModelState.IsValid)
            {
                db.Entry(user).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index", "Account");
            }
            return View(user);
        }

        // GET: Account/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = await db.Users.FindAsync(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: Account/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            User user = await db.Users.FindAsync(id);
            db.Users.Remove(user);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
