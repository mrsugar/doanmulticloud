﻿CREATE TABLE [dbo].[User]
(
	[UserID] INT NOT NULL PRIMARY KEY IDENTITY(1,1), 
    [Email] VARCHAR(MAX) NOT NULL, 
    [Password] VARCHAR(MAX) NOT NULL, 
    [Verified] BIT NOT NULL DEFAULT 0, 
    [Gender] BIT NOT NULL DEFAULT 0, 
    [createDate] DATETIME NOT NULL
)
