﻿CREATE TABLE [dbo].[Cloud]
(
	[CloudID] INT NOT NULL PRIMARY KEY IDENTITY(1,1), 
    [UserID] INT NOT NULL, 
    [CloudToken] VARCHAR(MAX) NOT NULL, 
    [CloudState] VARCHAR(MAX) NOT NULL, 
)
