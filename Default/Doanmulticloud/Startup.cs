﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Doanmulticloud.Startup))]
namespace Doanmulticloud
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
